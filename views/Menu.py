import tkinter as tk
from kernel.View import View


class Menu(View):

    def structure(self):
        my_image = tk.PhotoImage(file=self.model["path_image"])
        label = tk.Label(self, image=my_image)
        label.image = my_image
        label.pack()

        joueur = tk.StringVar()
        joueur.set("Player name")
        new_player = tk.Entry(self, textvariable=joueur, width=30)
        new_player.pack()

        new_game_noob = tk.Button(
            self,
            text=self.model["str_btn_new_game_noob"],
            command=lambda: self.create_game(joueur.get(), 1)
        )
        new_game_noob.pack()

        new_game_hard = tk.Button(
            self,
            text=self.model["str_btn_new_game_hard"],
            command=lambda: self.create_game(joueur.get(), 2)
        )
        new_game_hard.pack()

        classement_button = tk.Button(self, text=self.model["str_btn_score_board"], command=self.classement)
        classement_button.pack()

    def classement(self):
        self.path("show_score_board")

    def create_game(self, joueur, grille_num):
        self.path("start_game", **{
            "player_name": joueur,
            "difficulties": grille_num
        })
