import tkinter as tk
from kernel.View import View


class Classement(View):

    def structure(self):
        my_image = tk.PhotoImage(file=self.model["path_image"])
        label = tk.Label(self, image=my_image)
        label.image = my_image
        label.pack()
        for p in self.model["all_partie"]:
            if p.finis:
                titre_joueur = tk.Label(self, text="Joueur: %s | Temps: %ss" % (p.joueur.pseudo, p.temps.time))
                titre_joueur.pack(fill=tk.BOTH, side=tk.TOP)

        back_button = tk.Button(self, text=self.model["str_btn_back"], command=self.retour)
        back_button.pack()
        delete_result = tk.Button(self, text=self.model["str_btn_delete"], command=self.delete_results)
        delete_result.pack()

    def retour(self):
        self.path("show_menu")

    def delete_results(self):
        self.controller.delete_results()
