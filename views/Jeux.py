import tkinter as tk
from kernel.View import View


class Jeux(View):

    def structure(self):
        self.model["game"].start()
        self.__init_ui()

    def __init_ui(self):
        self.titre_joueur = tk.Label(self, text=self.model["str_btn_player"])
        self.titre_joueur.pack(fill=tk.BOTH, side=tk.TOP)

        self.canvas = tk.Canvas(self, width=self.model["WIDTH"], height=self.model["HEIGHT"])
        self.canvas.pack(fill=tk.BOTH, side=tk.TOP)

        self.clear_button = tk.Button(self, text=self.model["str_btn_clean"], command=self.__clear_answers)
        self.clear_button.pack(fill=tk.BOTH, side=tk.RIGHT)

        self.back = tk.Button(self, text=self.model["str_btn_back"], command=self.back_menu)
        self.back.pack(fill=tk.BOTH, side=tk.LEFT)

        self.undo = tk.Button(self, text=self.model["str_btn_undo"], command=self.__undo)
        self.undo.pack(fill=tk.BOTH, side=tk.RIGHT)

        self.__draw_grid()
        self.__draw_puzzle()

        self.canvas.bind("<Button-1>", self.__cell_clicked)
        self.canvas.bind("<Key>", self.__key_pressed)

    def back_menu(self):
        self.path("show_menu")

    def __undo(self):
        if self.model["previous_input"]:
            last_col = self.model["previous_input"][-1][0]
            last_row = self.model["previous_input"][-1][1]

            self.model["game"].puzzle[last_row][last_col] = 0
            self.model["previous_input"].remove([last_col, last_row])
            self.__draw_puzzle()
            self.__draw_cursor()
            if self.model["game"].check_win():
                self.__draw_victory()

    def __clear_answers(self):
        self.model["game"].start()
        self.canvas.delete("victory")
        self.__draw_puzzle()

    def __draw_grid(self):
        for i in range(10):
            color = "blue" if i % 3 == 0 else "gray"

            x0 = self.model["MARGIN"] + i * self.model["SIDE"]
            y0 = self.model["MARGIN"]
            x1 = self.model["MARGIN"] + i * self.model["SIDE"]
            y1 = self.model["HEIGHT"] - self.model["MARGIN"]
            self.canvas.create_line(x0, y0, x1, y1, fill=color)

            x0 = self.model["MARGIN"]
            y0 = self.model["MARGIN"] + i * self.model["SIDE"]
            x1 = self.model["WIDTH"] - self.model["MARGIN"]
            y1 = self.model["MARGIN"] + i * self.model["SIDE"]
            self.canvas.create_line(x0, y0, x1, y1, fill=color)

    def __draw_puzzle(self):
        self.canvas.delete("numbers")
        for i in range(9):
            for j in range(9):
                answer = self.model["game"].puzzle[i][j]
                if answer != 0:
                    x = self.model["MARGIN"] + j * self.model["SIDE"] + self.model["SIDE"] // 2
                    y = self.model["MARGIN"] + i * self.model["SIDE"] + self.model["SIDE"] // 2
                    original = self.model["game"].start_puzzle[i][j]
                    color = "black" if answer == original else "sea green"
                    self.canvas.create_text(x, y, text=answer, tags="numbers", fill=color)

    def __draw_cursor(self):
        self.canvas.delete("cursor")
        if self.model["row"] >= 0 and self.model["col"] >= 0:
            x0 = self.model["MARGIN"] + self.model["col"] * self.model["SIDE"] + 1
            y0 = self.model["MARGIN"] + self.model["row"] * self.model["SIDE"] + 1
            x1 = self.model["MARGIN"] + (self.model["col"] + 1) * self.model["SIDE"] - 1
            y1 = self.model["MARGIN"] + (self.model["row"] + 1) * self.model["SIDE"] - 1
            self.canvas.create_rectangle(
                x0, y0, x1, y1,
                outline="red", tags="cursor"
            )

    def __draw_victory(self):
        x0 = y0 = self.model["MARGIN"] + self.model["SIDE"] * 2
        x1 = y1 = self.model["MARGIN"] + self.model["SIDE"] * 7
        self.canvas.create_oval(
            x0, y0, x1, y1,
            tags="victory", fill="dark orange", outline="orange"
        )
        x = y = self.model["MARGIN"] + 4 * self.model["SIDE"] + self.model["SIDE"] // 2
        self.canvas.create_text(
            x, y,
            text="Vous avez fini en " + str(self.model["game"].partie.temps.time) + " secondes !", tags="victory",
            fill="white", font=("Arial", 20)
        )

    def __cell_clicked(self, event):
        if self.model["game"].game_over:
            self.path("show_menu")
            return
        x, y = event.x, event.y
        if (self.model["MARGIN"] < x < self.model["WIDTH"] - self.model["MARGIN"] and
                self.model["MARGIN"] < y < self.model["HEIGHT"] - self.model["MARGIN"]):
            self.canvas.focus_set()
            row = (y - self.model["MARGIN"]) // self.model["SIDE"]
            col = (x - self.model["MARGIN"]) // self.model["SIDE"]
            if (row, col) == (self.model["row"], self.model["col"]):
                self.model["row"], self.model["col"] = -1, -1
            elif self.model["game"].puzzle[row][col] == 0:
                self.model["row"], self.model["col"] = row, col
        else:
            self.model["row"], self.model["col"] = -1, -1

        self.__draw_cursor()

    def __key_pressed(self, event):
        if self.model["game"].game_over:
            self.path("show_menu")
            return
        available_num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
        if self.model["row"] >= 0 and self.model["col"] >= 0 and event.char in available_num:
            self.model["game"].puzzle[self.model["row"]][self.model["col"]] = int(event.char)
            self.model["game"].save_grille_joueur()
            self.model["previous_input"].append([self.model["col"], self.model["row"]])
            self.model["col"], self.model["row"] = -1, -1
            self.__draw_puzzle()
            self.__draw_cursor()
            if self.model["game"].check_win():
                self.__draw_victory()
