from peewee import *
from services.DatabaseAccess import DatabaseAccess


db = DatabaseAccess().get_db()


class Joueur(Model):
    pseudo = CharField()

    class Meta:
        database = db


class Temps(Model):
    time = FloatField()

    class Meta:
        database = db


class GrilleBase(Model):
    contenue = TextField()

    class Meta:
        database = db


class GrilleJoueur(Model):
    contenue = TextField()

    class Meta:
        database = db


class Partie(Model):
    joueur = ForeignKeyField(Joueur, backref='joueur')
    grille_base = ForeignKeyField(GrilleBase, backref="grille_base")
    grille_save = ForeignKeyField(GrilleJoueur, backref="grille_save")
    temps = ForeignKeyField(Temps, backref="temps")
    finis = BooleanField(default=False)

    class Meta:
        database = db
