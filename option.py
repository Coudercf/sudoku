import inspect
import sys
import os
from models import model
from models.model import *

BLUE = "\33[94m"
RED = "\33[91m"
GREEN = "\33[92m"
END = "\033[0m"

if __name__ == "__main__":

    if "migrate" in sys.argv and sys.argv[1] == "migrate":
        print(BLUE + "Migrate" + END)
        path_db = os.path.join(os.getcwd(), "models", "sudoku.db")
        if os.path.exists(path_db):
            print(RED + "Remove previous DB" + END)
            os.remove(path_db)

        print(GREEN + "Create new DB" + END)
        db = DatabaseAccess().get_db()
        table_to_create = []
        for name, obj in inspect.getmembers(sys.modules[model.__name__], inspect.isclass):
            if obj.__module__ == "models.model":
                table_to_create.append(obj)
        db.connect()
        print(GREEN + "Create Tables for %s" % ", ".join(str(t) for t in table_to_create) + END)
        db.create_tables(table_to_create)
        grille_data = [
            {"contenue": "217385469\n385469712\n496721835\n524816973\n639547281\n871293546\n762158394\n953674128\n148932650"},
            {"contenue": "210000400\n380400702\n000720000\n024806900\n000000000\n001203540\n000058000\n903004028\n008000057"},
            {"contenue": "809200000\n200980160\n030007008\n008600500\n400000002\n003008400\n300400050\n045032006\n000006205"},
        ]
        print(GREEN + "Create default content in Grille" + END)
        GrilleBase.insert_many(grille_data).execute()
        print(BLUE + "Finish !" + END)

    if "compile" in sys.argv and sys.argv[1] == "compile":
        if len(sys.argv) > 2:
            script = os.path.join(os.getcwd(), sys.argv[2])
            if os.path.exists(script):
                command = "pyinstaller %s %s" % (script, " ".join(sys.argv[3:]))
                print(command)
                os.system(command)
            else:
                print("%s n'existe pas" % script)
        else:
            print("un script est nécessaire pour compiler: \"python option.py compile 'nom_application.py'\"")
