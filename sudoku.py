from kernel.Application import Application


class Sudoku(Application):

    BOARDS = ['debug', 'n00b', 'l33t', 'error']
    MARGIN = 20
    SIDE = 60
    WIDTH = HEIGHT = MARGIN * 2 + SIDE * 9

    def __init__(self):
        super(Sudoku, self).__init__("show_menu")
        self.start("Sudoku")


if __name__ == "__main__":
    app = Sudoku()
    app.title = "Sudoku"
    app.geometry("%dx%d" % (app.WIDTH, app.HEIGHT + 60))
    app.mainloop()
