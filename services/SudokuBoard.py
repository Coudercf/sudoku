from errors import SudokuError


class SudokuBoard:

    __instance = None

    def __new__(cls, *args, **kwargs):
        if SudokuBoard.__instance is None:
            SudokuBoard.__instance = object.__new__(cls)
        return SudokuBoard.__instance

    def create_board(self, grille):
        new_board = []

        for line in grille.contenue.split("\n"):
            line = line.strip()

            if len(line) != 9:
                raise SudokuError("Chaque lignes du Sudoku doivent faire 9 chiffre de long")

            new_board.append([])

            for c in line:
                if not c.isdigit():
                    raise SudokuError("Le Sudoku ne doit comporter que des chiffres (0-9)")
                new_board[-1].append(int(c))

        if len(new_board) != 9:
            raise SudokuError("Le Sudoku doit faire 9 lignes")

        return new_board
