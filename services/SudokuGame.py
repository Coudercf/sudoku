from services.SudokuBoard import SudokuBoard
from services.TimerClass import TimerClass


class SudokuGame:

    __instance = None

    def __new__(cls, *args, **kwargs):
        if SudokuGame.__instance is None:
            SudokuGame.__instance = object.__new__(cls)
        return SudokuGame.__instance

    def __init__(self, partie):
        self.timer = None
        self.game_over = False
        self.partie = partie
        self.start_puzzle = SudokuBoard().create_board(self.grille_base)
        self.puzzle = []

    @property
    def grille_base(self):
        return self.partie.grille_base

    @property
    def grille_joueur(self):
        return self.partie.grille_save

    def save_grille_joueur(self):
        contenue = ""
        for line in self.puzzle:
            contenue += "".join([str(l) for l in line]) + "\n"
        self.grille_joueur.contenue = contenue
        self.grille_joueur.save()

    def start(self):
        self.game_over = False
        self.partie.finis = False
        self.partie.save()
        self.grille_joueur.contenue = self.grille_base.contenue
        self.grille_joueur.save()
        self.puzzle = SudokuBoard().create_board(self.grille_joueur)
        self.timer = TimerClass()
        self.timer.count = 0
        self.timer.start()

    def check_win(self):
        for row in range(9):
            if not self.__check_row(row):
                return False
        for column in range(9):
            if not self.__check_column(column):
                return False
        for row in range(3):
            for column in range(3):
                if not self.__check_square(row, column):
                    return False
        self.game_over = True
        self.partie.finis = True
        self.partie.save()
        self.partie.temps.time = self.timer.count
        self.partie.temps.save()
        self.timer.count = 0
        return True

    def __check_block(self, block):
        return set(block) == set(range(1, 10))

    def __check_row(self, row):
        return self.__check_block(self.puzzle[row])

    def __check_column(self, column):
        return self.__check_block(
            [self.puzzle[row][column] for row in range(9)]
        )

    def __check_square(self, row, column):
        return self.__check_block([
            self.puzzle[r][c]
            for r in range(row * 3, (row + 1) * 3)
            for c in range(column * 3, (column + 1) * 3)
        ])
