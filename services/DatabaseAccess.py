import os
import sys
from peewee import *


class DatabaseAccess(object):

    sudoku_database = None
    path_db = os.path.join("models", "sudoku.db")
    __instance = None

    def __new__(cls):
        if DatabaseAccess.__instance is None:
            DatabaseAccess.__instance = object.__new__(cls)
        if DatabaseAccess.sudoku_database is None:
            DatabaseAccess.sudoku_database = SqliteDatabase(cls.relative_path(DatabaseAccess.path_db))
        return DatabaseAccess.__instance

    def get_db(self):
        return self.sudoku_database

    @staticmethod
    def relative_path(relative_path):
        try:
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")
        return os.path.join(base_path, relative_path)
