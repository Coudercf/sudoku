import os
from tkinter import messagebox
from services import SudokuGame
from kernel.Controller import Controller


class Sudoku(Controller):

    def __init__(self, *args, **kwargs):
        super(Sudoku, self).__init__(*args, **kwargs)
        self.data_access = self.get_service("DatabaseAccess")

    def show_menu(self):
        path_image = self.relative_path(os.path.join("images", "DopeyDeadlyCusimanse-size_restricted.gif"))
        str_btn_new_game_noob = "Nouvelle partie Facile"
        str_btn_new_game_hard = "Nouvelle partie Difficile"
        str_btn_score_board = "Classement"
        self.show_frame("Menu", **{
            "path_image": path_image,
            "str_btn_new_game_noob": str_btn_new_game_noob,
            "str_btn_new_game_hard": str_btn_new_game_hard,
            "str_btn_score_board": str_btn_score_board
        })

    def show_score_board(self):
        partie = self.get_model("Partie")
        temps = self.get_model("Temps")
        path_image = self.relative_path(os.path.join("images", "DopeyDeadlyCusimanse-size_restricted.gif"))
        all_partie = partie.select().join(temps).order_by(partie.temps.time.asc())
        str_btn_back = "Retour"
        str_btn_delete = "Réinitialisé les Scores"
        self.show_frame("Classement", **{
            "path_image": path_image,
            "all_partie": all_partie,
            "str_btn_back": str_btn_back,
            "str_btn_delete": str_btn_delete
        })

    def start_game(self, player_name, difficulties):
        grille_base = self.get_model("GrilleBase")
        grille_joueur = self.get_model("GrilleJoueur")
        temps = self.get_model("Temps")
        joueur = self.get_model("Joueur")
        partie = self.get_model("Partie")

        grille = grille_base[difficulties]
        nouveau_joueur, created = joueur.get_or_create(pseudo=player_name)
        nouveau_joueur.save()

        parties = partie.select().where(partie.joueur == nouveau_joueur)
        new_game = None
        for game in parties:
            if game.finis == False:
                answer = messagebox.askquestion(
                    "Partie en cour",
                    "Une partie est en cour, voulez-vous continuer ?"
                )
                if answer == messagebox.YES:
                    new_game = game
                    break
                else:
                    game.delete_instance()
        if not new_game:
            new_temps = temps(time=0.0)
            new_temps.save()
            new_grille = grille_joueur(contenue=grille.contenue)
            new_grille.save()
            new_game = partie(joueur=nouveau_joueur, grille_base=grille, grille_save=new_grille, temps=new_temps)
            new_game.save()

        self.show_frame("Jeux", **{
            "row": -1,
            "col": -1,
            "last_row": -1,
            "last_col": -1,
            "previous_input": list(),
            "game": SudokuGame(new_game),
            "WIDTH": self.application.WIDTH,
            "HEIGHT": self.application.HEIGHT,
            "SIDE": self.application.SIDE,
            "MARGIN": self.application.MARGIN,
            "str_btn_player": "Joueur: %s" % nouveau_joueur.pseudo,
            "str_btn_clean": "Nettoyer",
            "str_btn_back": "Retour Menu",
            "str_btn_undo": "Annuler"
        })

    def delete_results(self):
        partie = self.get_model('Partie')
        temps = self.get_model("Temps")
        partie_finish = partie.select().join(temps)
        for partie in partie_finish:
            if partie.finis:
                print('delete partie:', partie.joueur.pseudo)
                partie.temps.delete_instance()
                partie.delete_instance()

        self.show_score_board()
