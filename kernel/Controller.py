import os
import sys
import tkinter as tk


class Controller:

    def __init__(self, application, *args, **kwargs):
        self.__application = application

    @property
    def application(self):
        return self.__application

    def show_frame(self, page_name, *args, **kwargs):
        if self.application.current_view:
            self.application.current_view.destroy()
        new_frame = self.application.views[page_name](self, *args, **kwargs)
        self.application.current_view = new_frame
        self.application.current_view.pack(fill=tk.BOTH)

    def get_service(self, name: str):
        return self.application.get_service(name)

    def get_model(self, name: str):
        return self.application.get_model(name)

    def relative_path(self, relative_path):
        try:
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")
        return os.path.join(base_path, relative_path)
