import tkinter as tk
from kernel.errors.RouteError import RouteError


class View(tk.Frame):

    def __init__(self, controller, *args, **kwargs):
        tk.Frame.__init__(self)
        self.controller = controller
        self.canvas = None
        self.model = {}
        for name, value in kwargs.items():
            self.model[name] = value
        self.args = args
        self.structure()

    def path(self, name, *args, **kwargs):
        try:
            getattr(self.controller, name)(*args, **kwargs)
        except AttributeError:
            raise RouteError("%s doesn't have route: %s" % (self.controller.__name__, name))

    def structure(self):
        raise NotImplementedError
