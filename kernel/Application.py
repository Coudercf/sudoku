import sys
import views
import inspect
import models
import services
import controllers
import tkinter as tk
from tkinter import messagebox
from kernel.errors.RouteError import RouteError


class Application(tk.Tk):

    def __init__(self, start_route: str, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.start_route = start_route
        self.views = {}
        self.models = {}
        self.services = {}
        self.controllers = {}
        self.current_view = None
        self.current_controller = None
        self.__init_views()
        self.__init_models()
        self.__init_services()
        self.__init_controllers()
        self.protocol("WM_DELETE_WINDOW", self.close_app)

    def get_views(self, name):
        return self.views[name]

    def get_model(self, name):
        return self.models[name]

    def get_service(self, name):
        return self.services[name]

    def __init_views(self):
        self.__inspect_module(views.__name__, self.views)

    def __init_models(self):
        self.__inspect_module(models.__name__, self.models)

    def __init_services(self):
        self.__inspect_module(services.__name__, self.services)

    def __init_controllers(self):
        self.__inspect_module(controllers.__name__, self.controllers)

    def __inspect_module(self, name: str, storage: dict):
        for name_obj, obj in inspect.getmembers(sys.modules[name], inspect.isclass):
            if name in obj.__module__:
                storage[name_obj] = obj

    def start(self, name):
        controller = self.controllers[name](self)
        self.current_controller = controller
        try:
            start_route = getattr(self.current_controller, self.start_route)
        except AttributeError:
            raise RouteError("%s doesn't have route: %s" % (self.current_controller.__name__, self.start_route))
        start_route()

    def close_app(self):
        if messagebox.askokcancel("Quit", "Voulez-vous quitter l'application ?"):
            self.destroy()
